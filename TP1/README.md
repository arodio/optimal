# TP1: 08/03/2022 (13:30 - 16:45). Room: Lucioles, 352.

## Introduction to the course
Teachers: [Angelo Rodio](http://www-sop.inria.fr/members/Angelo.Rodio/), [Othmane Marfoq](http://www-sop.inria.fr/members/Othmane.Marfoq/). 
Email: <angelo.rodio@inria.fr>.

## 1) Tutorial. Gradient Descent and Momentum: The Heavy Ball Method. [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1T8Q9SZCKbtejgZPyaY0d0AlegXzm1P-d?usp=sharing)
Implement GD in Numpy. Discuss about pathological curvature. Describe how momentum can help address the problems.

## 2) Introduction to PyTorch:
- Module: torch.autograd and torch.optim. Figure: Computational graphs in PyTorch. [![Open In Draw.io](https://raw.githubusercontent.com/jgraph/drawio/36cd0b3a09e6ac654b983ad7c626a6aa0a4d4a38/src/main/webapp/images/draw.io-logo.svg)](https://drive.google.com/file/d/1GnHcvQMhIp4MzwhU17bEy_rBHue4NG7Q/view?usp=sharing)
- Tutorial: refactor the code "Gradient Descent and Momentum" in PyTorch. [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1CdMhyYG5u6a_poquqhCxwZIqym-xDXwr?usp=sharing)
- Module: Datasets and DataLoaders. [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1iwqu8_LJURRU7O-sbIBbpBiyahqnECng?usp=sharing)
- Module: torch.nn. Tutorial: Build a CNN and move to GPU. [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/14oFwbShJQZ9ihHdRfeTKcQ0HwcML_fGH?usp=sharing)
- Break (15 min). Enjoy!

## 3) Tutorial. Configuration of the Inria cluster. Introduction to torch.distributed. [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1aZir6cn_X_fgzcN8DOmWb7n38yfqXzSB?usp=sharing)
The torch.distributed package provides PyTorch support and communication primitives for multiprocess parallelism across several computation nodes running on one or more machines. We will use the Inria scientific cluster to run our code on multiple GPUs.
