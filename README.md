# Optimization for Machine Learning, 2021-2022

## General info
This is the lab course for the main course: [Optimization for Machine Learning](http://www-sop.inria.fr/members/Giovanni.Neglia/optimal/).  
Teachers: [Othmane Marfoq](http://www-sop.inria.fr/members/Othmane.Marfoq/), [Angelo Rodio](http://www-sop.inria.fr/members/Angelo.Rodio/).    
During these practical sessions students will have the opportunity to train ML models in a distributed way on Inria scientific cluster.  
Participation to the labs will be graded by the teachers.

## Prerequites
Students need to carry out the following administrative/configuration steps before the start of the labs:
* [Tutorial for the usage of NEF cluster](https://gitlab.inria.fr/arodio/optimal/-/blob/main/NEF.pdf).

## Usage of Nef cluster 
1) log in account: run `ssh user@nef-frontal.inria.fr` (for people using Windows, you may need to use PuTTy)
2) reserve computing resources; for example a mode with one core. run `oarsub -l /nodes=1/core=1,walltime=3 -I`
3) run `module load conda/5.0.1-python3.6`
4) run `conda create -n optimal`
5) run `conda activate optimal`. If it does not work, run `source activate optimal`
6) run `module load cuda/11.0`  
7) run `conda install pytorch==1.7.1 torchvision==0.8.2 cudatoolkit=11.0 -c pytorch`

## Class schedule
Lessons will be from 13.30 to 16.45.
* TP1: 08/03/2022. Room: Lucioles, 352.  
* TP2: 15/03/2022. Room: Lucioles, 352. 
* TP3: 22/03/2022. Room: TBD.

## Course material
[GitLab page of the course](https://gitlab.inria.fr/arodio/optimal).

