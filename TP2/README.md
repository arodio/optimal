# TP2: 15/03/2022 (16:30 - 16:45). Room: Lucioles, 352.

## Introduction to PyTorch (Part 2)
* Writing costum datasets and dataloaders [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1YKTSAkbmABtqZlQ5zuG7eoaaLTNR1ijA?usp=sharing)
* Handle `torch.nn.Module` object [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1xBdgFfTCbkf5rCHu8vxZUZbOmpbScjq-?usp=sharing)

## Introduction to `torch.distributed` [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1aZir6cn_X_fgzcN8DOmWb7n38yfqXzSB?usp=sharing)
The `torch.distributed` package provides PyTorch support and communication primitives for multiprocess parallelism across several computation nodes running on one or more machines. We will use the Inria scientific cluster to run our code on multiple GPUs.
